function [A,b,X] = generate(ixa,ixb,x0)
%Funkcija za generiranje nakljucnega ekosistema s tremi plenilci, dvema
%rastlinojedoma in rastlinami

 
%handlanje optional parametrov(handy stuff to have)
 if ~exist('ixa','var')
     % parameter does not exist, so default it to something
      ixa = 10;
 end
 if ~exist('ixb','var')
     % parameter does not exist, so default it to something
      ixb = 10;
 end
  if ~exist('x0','var')
     % parameter does not exist, so default it to something
      x0 = [8.7, 28.7, 122.3, 117.4, 13.0, 172.3]';
      
  end
 
%generiranje random matrike odnosov med vrstami
A = randi(ixa,6,6);
temp = [0,1,1,0,0,0;
        -1,0,0,1,0,0;
        -1,0,0,1,1,0;
        0,-1,-1,0,0,1;
        0,0,-1,0,0,1;
        0,0,0,-1,-1,0];
A = A.*temp*0.001;

%generiranje random b
b = randi(ixb,6,1)*0.001;

%funkcija sistema dif. enacb(vrne stolpicni vektor sprememb populacij vseh vrst)
F = @(X,b,A) X.*(b+A*X);


%runge kutta z memorizacijo vmesnih korakov
x = x0;
MEM = zeros(6,100);
for i = 1:5000
    %naredimo korak rk4
    x = x + rk4step(F,x,0.1,b,A);
    MEM(:,i) = x;
end


%izris zapomnjenih vrednosti za vsako vrsto posebej
hold on;
for i = 1:6
    plot(MEM(i,:));
end

X = x;
end

%funkcija, ki izracuna en korak po rk4 metodi
function k = rk4step(f,x,h,b,A)
k1 = h*f(x,b,A);
k2 = h*f(x+k1/2,b,A);
k3 = h*f(x+k2/2,b,A);
k4 = h*f(x+k3,b,A);

k = (k1+2*k2+2*k3+k4)/6;
end



% Primer A b in X za testiranje
% A = [0 0.5 0.7 0 0 0;
% -2 0 0 1 0 0;
% -8 0 0 1 4 0;
% 0 -6 -1 0 0 2;
% 0 0 -1 0 0 1;
% 0 0 0 -2 -5 0]*0.001;
% 
% b = [-0.1, -0.1, -0.1, -0.05, -0.05, 0.3]';
%
%Ta X vrne periodicno obnasanje:
% X =  [9.0, 32.6, 125.9, 121.0, 16.5, 175.4]';
